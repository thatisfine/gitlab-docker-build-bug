FROM alpine:latest AS buildstage1
RUN touch /testfile

FROM nginx:latest
COPY --from=buildstage1 /testfile /testfile
